package com.vincibean.fabexplosionanimation;

import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.vincibean.fabexplosionanimation.databinding.ActivityMainBinding;

import org.jetbrains.annotations.Nullable;

import kotlin.jvm.internal.Intrinsics;

public final class MainActivity extends AppCompatActivity {
    ActivityMainBinding binding;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(this.getLayoutInflater());
        Intrinsics.checkNotNullExpressionValue(binding, "ActivityMainBinding.inflate(layoutInflater)");

        this.setContentView(binding.getRoot());
        final Animation animation = AnimationUtils.loadAnimation(this, R.anim.circle_explosion_anim);
        animation.setDuration(700L);
        animation.setInterpolator((new AccelerateDecelerateInterpolator()));
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // display your fragment
                binding.getRoot().setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.purple_500));
                binding.circle.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        binding.fab.setOnClickListener(it -> {
            binding.fab.setVisibility(View.INVISIBLE);
            binding.circle.setVisibility(View.INVISIBLE);
            binding.circle.startAnimation(animation);
        });
    }

}
